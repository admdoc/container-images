ARG PYTHON_VERSION=3.11
FROM python:${PYTHON_VERSION}

ARG NODE_VERSION=18

ENV POETRY_HOME=/usr/local
# Install node prereqs, nodejs and yarn
# Ref: https://deb.nodesource.com/setup_{{ nodejs }}.x
# Ref: https://yarnpkg.com/en/docs/install

COPY nodesource.gpg /etc/apt/keyrings/nodesource.gpg

RUN \
  echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_${NODE_VERSION}.x nodistro main" > /etc/apt/sources.list.d/nodesource.list && \
  apt-get update && \
  apt-get install -yqq \
    nodejs \
    curl \
    unzip \
    && \
  wget -q "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -O "/tmp/awscliv2.zip" && \
  unzip -q /tmp/awscliv2.zip -d /tmp && \
  /tmp/aws/install && \
  pip install -U pip && pip install pipenv && \
  wget -qO- https://install.python-poetry.org | python - && \
  rm -rf /var/lib/apt/lists/* /tmp/aws /tmp/awscliv2.zip
